---
title : "Modern Documentation Theme"
description: "Doks is a Hugo theme helping you build modern documentation websites that are secure, fast, and SEO-ready — by default."
date: 2020-10-06T08:47:36+00:00
lastmod: 2020-10-06T08:47:36+00:00
draft: false
images: []
---

[Doks](https://github.com/h-enk/doks/) is a Hugo theme helping you build modern documentation websites that are secure, fast, and SEO-ready — by default. It [uses npm](https://henkverlinde.com/master-npm-with-hugo-managing-dependencies/) for dependencies management. 

[Doks](https://github.com/h-enk/doks/) is build by [Henk Verlinde](https://twitter.com/HenkVerlinde).

This site shows the use of Doks in book-like fashion, with left-hand TOC
appearing on front page. 

#### Translation 

The Russian version of the site is available [here](ru/).

The making of a translation involves a change of configuration files 
and adding new content files with text in your language. 
Check [this commit for details](https://gitlab.com/epogrebnyak/doks-howto/-/commit/9b1f6676db750fd18b5241f62793da9eab4428bc).

You can find entire the source of this site [here](https://gitlab.com/epogrebnyak/doks-howto/).


#### About Doks theme on Twitter

{{< tweet 1344276759232073729 >}}